#include "SampleRaycastCar.h"
#include "Utilities.h"

extern NxSpringDesc wheelSpring;

extern NxUserContactReport * carContactReport;

extern NxMaterialIndex materialIce, materialRock, materialMud, materialGrass, materialDefault;

CreationModes		creationMode = MODE_CAR;
bool				gPause = false;
bool				gDebugVisualization = false;
bool				oldStyle = false;
NxPhysicsSDK*		gPhysicsSDK = 0;
NxScene*			gScene = 0;
NxVec3				gDefaultGravity(0.0f, -10.0f, 0.0f);
ErrorStream			gErrorStream;
DebugRenderer       gDebugRenderer;
bool keyDown[256];

// Focus actor
NxActor* gSelectedActor = NULL;

NxVec3 Eye(250, 50, 65);
NxVec3 Dir(-0.65,-0.53,-0.54);
NxVec3 CameraPos(250, 50, 65);
NxVec3 CameraDir(-0.65, -0.53, -0.54);
NxVec3 N;
NxI32 gLastVehicleNumber = 0;

NxMaterialIndex defaultMaterialIndex;
NxMaterialIndex somewhatBouncyMaterialIndex;
NxMaterialIndex veryBouncyMaterialIndex;
NxMaterialIndex frictionlessMaterialIndex;
NxMaterialIndex highFrictionMaterialIndex;
NxMaterialIndex anisoMaterialIndex;

NxPhysicsSDK* getPhysxSDK()
{
	return gPhysicsSDK;
}

NxScene* getgScene()
{
	return gScene;
}

void CreateCube(const NxVec3& pos, const NxVec3* initial_velocity)
{
	// Create body
	NxBodyDesc BodyDesc;
	if(initial_velocity)	BodyDesc.linearVelocity = *initial_velocity;

	NxBoxShapeDesc boxDesc;
	NxReal size = 0.5f;
	boxDesc.dimensions		= NxVec3(size, size, size);
	BodyDesc.mass = 10.f;

	NxActorDesc ActorDesc;
	ActorDesc.shapes.pushBack(&boxDesc);
	ActorDesc.body			= &BodyDesc;
	ActorDesc.globalPose.t  = pos;
	gScene->createActor(ActorDesc);
}

bool InitNx()
{
	// Initialize PhysicsSDK
	NxPhysicsSDKDesc desc;
	NxSDKCreateError errorCode = NXCE_NO_ERROR;
	gPhysicsSDK = NxCreatePhysicsSDK(NX_PHYSICS_SDK_VERSION, NULL, &gErrorStream, desc, &errorCode);
	if(gPhysicsSDK == NULL) 
	{
		printf("\nSDK create error (%d - %s).\nUnable to initialize the PhysX SDK, exiting the sample.\n\n", errorCode, getNxSDKCreateError(errorCode));
		return false;
	}

	if (!InitCooking(NULL, &gErrorStream)) {
		printf("\nError: Unable to initialize the cooking library, exiting the sample.\n\n");
		return false;
	}

	gPhysicsSDK->setParameter(NX_SKIN_WIDTH, 0.025f);
	//enable visualisation
	gPhysicsSDK->setParameter(NX_VISUALIZATION_SCALE, 1.0f);

	gPhysicsSDK->setParameter(NX_VISUALIZE_BODY_AXES, 1.0f);
	gPhysicsSDK->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1.0f);
	gPhysicsSDK->setParameter(NX_VISUALIZE_COLLISION_AXES, 1.0f);
	gPhysicsSDK->setParameter(NX_VISUALIZE_CONTACT_POINT, 1.0f);
	gPhysicsSDK->setParameter(NX_VISUALIZE_CONTACT_NORMAL, 1.0f);

	gPhysicsSDK->setParameter(NX_VISUALIZE_JOINT_LOCAL_AXES, 1.0f);
	gPhysicsSDK->setParameter(NX_VISUALIZE_JOINT_WORLD_AXES, 1.0f);
	gPhysicsSDK->setParameter(NX_VISUALIZE_JOINT_LIMITS, 1.0f);

	// Don't slow down jointed objects
	gPhysicsSDK->setParameter(NX_ADAPTIVE_FORCE, 0.0f);
	
	//create some materials -- note that we reuse the same NxMaterial every time,
	//as it gets copied to the SDK with every setMaterial() call, not just referenced.
	
	// Create a scene
	NxSceneDesc sceneDesc;
	sceneDesc.gravity				= gDefaultGravity;
	sceneDesc.userContactReport		= carContactReport;
	gScene = gPhysicsSDK->createScene(sceneDesc);
	if(gScene == NULL) 
	{
		printf("\nError: Unable to create a PhysX scene, exiting the sample.\n\n");
		return false;
	}
	
	//default material
	defaultMaterialIndex = 0;
	NxMaterial * defaultMaterial = gScene->getMaterialFromIndex(0); 
	defaultMaterial->setRestitution(0.0f);
	defaultMaterial->setStaticFriction(0.8f);
	defaultMaterial->setDynamicFriction(0.8f);

	//create ground plane
	NxActorDesc actorDesc;
	//embed some simple shapes in terrain to make sure we can drive on them:
	NxBoxShapeDesc boxDesc;
	boxDesc.dimensions.set(10,1,5);
	boxDesc.localPose.t.set(30,0,30);
	boxDesc.localPose.M.fromQuat(NxQuat(-30, NxVec3(0,0,1)));
	actorDesc.shapes.pushBack(&boxDesc);
	gScene->createActor(actorDesc);

	actorDesc.setToDefault();
	NxSphereShapeDesc sphereShape;
	sphereShape.radius = 20;
	sphereShape.localPose.t.set(140, -18, 0);
	actorDesc.shapes.pushBack(&sphereShape);
	gScene->createActor(actorDesc);

	actorDesc.setToDefault();
	NxCapsuleShapeDesc capsuleShape;
	capsuleShape.radius = 10.0f;
	capsuleShape.height = 10.0f;
	capsuleShape.localPose.t.set(100, -8, 0);
	capsuleShape.localPose.M.setColumn(1, NxVec3(0,0,1));
	capsuleShape.localPose.M.setColumn(2, NxVec3(0,-1,0));
	actorDesc.shapes.pushBack(&capsuleShape);

	gScene->createActor(actorDesc);

	// Terrain
	InitTerrain();

	// Turn on all contact notifications:
	gScene->setActorGroupPairFlags(0, 0, NX_NOTIFY_ON_TOUCH);

	glColor4f(1.0f,1.0f,1.0f,1.0f);

	return true;
}

void getCurrentPosAndDirection(NxVec3& pos, NxVec3& direction) 
{
	if(NxAllVehicles::getActiveVehicle() != NULL)
	{
		pos = CameraPos;
		direction = CameraDir;
	}
	else
	{
		pos = Eye;
		direction = Dir;
	}

}

void RenderAllActors()
{
	glColor4f(0.6f,0.6f,0.6f,1.0f);

	for(unsigned int i=0;i<gScene->getNbActors();i++)
	{
		ObjectRenderer::getSingletonRef().drawActor( gScene->getActors()[ i ] );
	}
}

void ReshapeCallback(int width, int height)
{
	glViewport(0, 0, width, height);
}

void ExitCallback()
{
	if (gPhysicsSDK)
	{
		if (gScene) gPhysicsSDK->releaseScene(*gScene);
		gPhysicsSDK->release();
	}
}



void appKey(unsigned char key, bool down)
{
	if (!down)
		return;
	bool alt = (glutGetModifiers() & GLUT_ACTIVE_ALT) > 0;

	switch(key)
	{
		case 27:	exit(0); break;
		case 'p':	gPause = !gPause; break;
		case 'f':	if (NxAllVehicles::getActiveVehicle()) NxAllVehicles::getActiveVehicle()->standUp(); 
					break;
		case 'v':	gDebugVisualization = !gDebugVisualization; break;

		case '+':	if (NxAllVehicles::getActiveVehicle()) NxAllVehicles::getActiveVehicle()->gearUp(); 
					break;
		case '-':	if (NxAllVehicles::getActiveVehicle()) NxAllVehicles::getActiveVehicle()->gearDown(); 
					break;
		case 'e':
			{
			NxAllVehicles::selectNext();
			} break;
		case 'r':
			{
			NxVec3 t;
			NxVec3 vel;
			getCurrentPosAndDirection(t, vel);
			
			vel.normalize();
			vel*=30.0f;
			CreateCube(t, &vel);
			}
			break;
		case 'c': {
			if (NxAllVehicles::getActiveVehicleNumber() < 0) {
				NxAllVehicles::setActiveVehicle(gLastVehicleNumber);
			} else {
				gLastVehicleNumber = NxMath::max(0, NxAllVehicles::getActiveVehicleNumber());
				NxAllVehicles::setActiveVehicle(-1);
			}
			break;
		}
	}
}


void RenderCallback(int _width, int _height)
{

	static unsigned int PreviousTime = 0;
	unsigned int CurrentTime = timeGetTime();
	unsigned int ElapsedTime = CurrentTime - PreviousTime;
	if(ElapsedTime < 10.0f) return;

	PreviousTime = CurrentTime;

	// Clear buffers -- do it now so we can render some debug stuff in tickCar.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Physics code
	if(gScene && !gPause)	
	{
		//tickCar();
		NxReal steering = 0;
		bool left = keyDown['a'] || keyDown[20];
		bool right = keyDown['d'] || keyDown[22];
		bool forward = keyDown['w'] || keyDown[21];
		bool backward = keyDown['s'] || keyDown[23];
		if (left && !right) steering = -1;
		else if (right && !left) steering = 1;
		NxReal acceleration = 0;
		if (forward && !backward) acceleration = 1;
		else if (backward && !forward) acceleration = -1;

		if(NxAllVehicles::getActiveVehicle())
			NxAllVehicles::getActiveVehicle()->control(steering, false, acceleration, false, false);
		NxAllVehicles::updateAllVehicles(1.0f/30.f);

		gScene->simulate(1.0f/30.0f);	//Note: a real application would compute and pass the elapsed time here.
		gScene->flushStream();
		gScene->fetchResults(NX_RIGID_BODY_FINISHED, true);
	}
	// ~Physics code
	
	// Setup camera
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (float)_width / (float)_height, 1.0f, 10000.0f);
	
	if(NxAllVehicles::getActiveVehicle() != NULL)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		#define BUFFERSIZE 25
		static NxMat34 mbuffer[BUFFERSIZE];
		static int frameNum = 0;
		int index = frameNum % BUFFERSIZE;
  	
		NxMat34 camera, cameraInv;
		NxVec3 cameraZ, lookAt, cr, tmp;
		NxF32 cameraInvMat[16];

		NxMat34 car = NxAllVehicles::getActiveVehicle()->getGlobalPose();
	  
		car.M.getColumn(2, cr);
		car.M.getColumn(0, tmp);
		car.M.setColumn(0, cr);
		car.M.setColumn(2, -tmp);

		if(frameNum == 0)
		{
			for(int i=0; i<BUFFERSIZE; i++)
				mbuffer[i] = car;
		}

		camera = mbuffer[index];
		mbuffer[index] = car;

		camera.t.y += NxAllVehicles::getActiveVehicle()->getCameraDistance() * 0.5f;	//camera height

		camera.M.getColumn(2, cameraZ);
		camera.t += (NxAllVehicles::getActiveVehicle()->getCameraDistance() * cameraZ);

		lookAt = (camera.t - car.t);
		lookAt.normalize();

		camera.M.setColumn(2, lookAt);
		cr = NxVec3(0,1,0).cross(lookAt);
		cr.normalize();
		camera.M.setColumn(0, cr);
		cr = lookAt.cross(cr);
		cr.normalize();
		camera.M.setColumn(1, cr);

		camera.getInverse(cameraInv);

		cameraInv.getColumnMajor44(cameraInvMat);

		glMultMatrixf(cameraInvMat);

		CameraPos = camera.t;
		CameraDir = -lookAt;

		frameNum++;
	}
	else
	{
		//camera controls:
		if (keyDown['a'] || keyDown[20]) Eye -= N;
		if (keyDown['d'] || keyDown[22]) Eye += N;
		if (keyDown['w'] || keyDown[21]) Eye += Dir;
		if (keyDown['s'] || keyDown[23]) Eye -= Dir;
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(Eye.x, Eye.y, Eye.z, Eye.x + Dir.x, Eye.y + Dir.y, Eye.z + Dir.z, 0.0f, 1.0f, 0.0f);
	}

	//render ground plane:
	glColor4f(0.4f, 0.4f, 0.4f, 1.0f); 
	if(!gDebugVisualization)
	{
	/*
		glPushMatrix();
		glScalef(10, 0, 10);
		glutSolidCube(600.0f);
		glPopMatrix();
	*/
	} else {

		static NxVec3* pPlaneVertexBuffer = NULL;
		
		if(pPlaneVertexBuffer == NULL)
		{
			pPlaneVertexBuffer = new NxVec3[200*4];
			for(int i=0;i<200;i++)
			{
				float v = (float)i*2-200.0f;
				pPlaneVertexBuffer[i*4+0].set(v,0.05f,-200.0f);
				pPlaneVertexBuffer[i*4+1].set(v,0.05f,200.0f);
				pPlaneVertexBuffer[i*4+2].set(-200.0f,0.05f,v);
				pPlaneVertexBuffer[i*4+3].set(200.0f,0.05f,v);
			}
		}
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3, GL_FLOAT, sizeof(NxVec3), pPlaneVertexBuffer);
		glDrawArrays(GL_LINES, 0, 200*4);
		glDisableClientState(GL_VERTEX_ARRAY);
	}

	glPushMatrix();

	RenderTerrain();

	if(gDebugVisualization) 
	{
		gDebugRenderer.renderData(*gScene->getDebugRenderable());
	} 
	else 
	{
		RenderAllActors();
	}

	glPopMatrix();
	NxAllVehicles::drawVehicles(gDebugVisualization);

	static CreationModes _oldCreationMode = MODE_NONE;
	static OrthographicDrawing orthoDraw;
	static float timeStamp = 0;

	orthoDraw.setOrthographicProjection( (float)_width, (float)_height );
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	if(_oldCreationMode != creationMode)
	{
		_oldCreationMode = creationMode;
		timeStamp=getCurrentTime();
	}

	renderHUD( orthoDraw, _width, _height );
	
	
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	orthoDraw.resetPerspectiveProjection();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);

}




void callback_key(unsigned char c, int x, int y)
{
	if(c >= 'A' && c <= 'Z') c = c - 'A' + 'a';
	keyDown[c] = true;
	appKey(c, true);
}

void callback_keyUp(unsigned char c, int x, int y)
{
	if(c >= 'A' && c <= 'Z') c = c - 'A' + 'a';
	keyDown[c] = false;
	appKey(c,false);
}