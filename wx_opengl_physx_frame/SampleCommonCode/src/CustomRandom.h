#ifndef __CUSTOM_RANDOM_H__
#define __CUSTOM_RANDOM_H__

// based on The Open Group Base Specifications Issue 6

class CustomRandom
{
	unsigned long next;

public:

	static const unsigned int rand_max = 32767;

	CustomRandom(unsigned seed = 1)
	{
		next = 0;
		srand(seed);
	}

	int rand(void)  /* RAND_MAX assumed to be 32767. */
	{
	    next = next * 1103515245 + 12345;
	    return((unsigned)(next/65536) % 32768);
	}

	void srand(unsigned seed)
	{
	    next = seed;
	}
};

#endif // __CUSTOM_RANDOM_H__