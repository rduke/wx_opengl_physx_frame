

#ifndef __DEBUGRENDER_H__
#define __DEBUGRENDER_H__

class NxDebugRenderable;

class DebugRenderer
{
public:
	void renderData(const NxDebugRenderable& data) const;

private:
	static void renderBuffer(float* pVertList, float* pColorList, int type, int num);
};

#endif // __DEBUGRENDER_H__


