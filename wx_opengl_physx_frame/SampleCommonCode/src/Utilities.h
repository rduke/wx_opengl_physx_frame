
#ifndef UTILITIES_H
#define UTILITIES_H

#if defined(WIN32)
#include <direct.h>
#include <shlwapi.h>
#endif

#if defined(__CELLOS_LV2__)
#define __forceinline inline __attribute__((always_inline))
#elif defined(LINUX)
#define __forceinline inline __attribute__((always_inline))
#endif

#include <Nx.h>

const char* getNxSDKCreateError(const NxSDKCreateError& errorCode);


static void RemoveFileFromPath(char *path);

static void SetCWDToEXE(void);

NX_INLINE bool isProcessorBigEndian();

// PT: those functions are useful for internal profiling during dev. They should not be used
// in the final code, so don't bother porting them to other platforms.

	//!	This function starts recording the number of cycles elapsed.
	//!	\param		val		[out] address of a 32 bits value where the system should store the result.
	//!	\see		EndProfile
	//!	\see		InitProfiler
	void	StartProfile(unsigned int& val);

	//!	This function ends recording the number of cycles elapsed.
	//!	\param		val		[out] address to store the number of cycles elapsed since the last StartProfile.
	//!	\see		StartProfile
	//!	\see		InitProfiler
	void	EndProfile(unsigned int& val);

#endif
