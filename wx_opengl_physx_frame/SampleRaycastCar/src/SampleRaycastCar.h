#ifndef __SAMPLE_RAYCAST_CAR_H__
#define __SAMPLE_RAYCAST_CAR_H__

#define NOMINMAX 

#if defined(WIN32)
#include <windows.h>
#include <GL/gl.h>
#include <GL>
#elif defined(__APPLE__)
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#endif
#include <stdio.h>

#include "OrthographicDrawing.h"
#include "NxAllVehicles.h"
#include "Stream.h"

// Physics code
#undef random
#include "NxPhysics.h"
#include "NxVehicle.h"
#include "ErrorStream.h"
#include "DebugRenderer.h"
#include "Timing.h"
#include "Terrain.h"
#include "ObjectRenderer.h"
#include "NxAllVehicles.h"

#include "cooking.h"

#ifndef WIN32
#include <unistd.h> // FIXME: ¿This work/necessary in Windows?
                    //Not necessary, but if it was, it needs to be replaced by process.h AND io.h
#endif

#define GL_COLOR_WHITE		1.0f,1.0f,1.0f,1.0f
#define GL_COLOR_RED		1.0f,0.0f,0.0f,1.0f
#define GL_COLOR_GREEN		0.0f,1.0f,0.0f,1.0f
#define GL_COLOR_BLUE		0.0f,0.0f,1.0f,1.0f
#define GL_COLOR_LIGHT_BLUE 0.5f,0.5f,1.0f,1.0f
#define GL_COLOR_LIGHT_GREY 0.7f,0.7f,0.7f,1.0f
#define GL_COLOR_BLACK		0.0f,0.0f,0.0f,1.0f



enum CreationModes { MODE_NONE, MODE_CAR, MODE_TRUCK };

NxPhysicsSDK*	getPhysxSDK();
NxScene*		getgScene();


//void initCar();
//void createCar(const NxVec3& pos);
void createCarWithDesc(const NxVec3& pos, bool frontWheelDrive, bool backWheelDrive, bool corvetteMotor, bool monsterTruck, bool oldStyle, NxPhysicsSDK* physicsSDK);
void createCart(const NxVec3& pos, bool frontWheelDrive, bool backWheelDrive, bool oldStyle);
NxVehicleDesc* createTruckPullerDesc(const NxVec3& pos, NxU32 nbGears, bool oldStyle);
NxVehicleDesc* createTruckTrailer1(const NxVec3& pos, NxReal length, bool oldStyle);
NxVehicleDesc* createFullTruckDesc(const NxVec3& pos, NxReal length, NxU32 nbGears, bool has4Axes, bool oldStyle);
NxVehicleDesc* createTwoAxisTrailer(const NxVec3& pos, NxReal length, bool oldStyle);

NxVehicle* createTruckPuller(const NxVec3& pos, NxU32 nbGears, bool oldStyle);
NxVehicle* createFullTruck(const NxVec3& pos, NxU32 nbGears, bool has4Axes, bool oldStyle);
NxVehicle* createTruckWithTrailer1(const NxVec3& pos, NxU32 nbGears, bool oldStyle);
NxVehicle* createFullTruckWithTrailer2(const NxVec3& pos, NxU32 nbGears, bool oldStyle);

bool InitNx();
void InitTerrain();
void RenderTerrain();
void RenderAllActors();
void renderHUD( OrthographicDrawing& orthoDraw, int _width, int _height );
void ExitCallback();
void ReshapeCallback(int width, int height);
void getCurrentPosAndDirection(NxVec3& pos, NxVec3& direction);
void CreateCube(const NxVec3& pos, const NxVec3* initial_velocity = NULL);
void appKey(unsigned char key, bool down);
void RenderCallback(int _width, int _height);
void callback_key(unsigned char c, int x, int y);
void callback_keyUp(unsigned char c, int x, int y);

#endif // __SAMPLE_RAYCAST_CAR_H__