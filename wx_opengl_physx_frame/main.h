#ifndef __MAIN_H__
#define __MAIN_H__

#include <wx/sizer.h>
#include <wx/wx.h>
#include <wx/timer.h>
#include <wx/glcanvas.h>
#include <GL>
#include <wx/dcbuffer.h>

class MyFrame;
 
class MyApp: public wxApp
{
    bool OnInit();
    
    MyFrame* frame;
public:

};

class wxGLCanvasSubClass;
 
class RenderTimer : public wxTimer
{
    wxGLCanvasSubClass* pane;
public:
    RenderTimer(wxGLCanvasSubClass* pane);
    void Notify();
    void start();
};

class wxGLCanvasSubClass: public wxGLCanvas
{
public:
    wxGLCanvasSubClass(wxFrame* parent);
	void paintEvent(wxPaintEvent& evt);
    void paintNow();
    void Render( wxDC& dc );
    void Paintit(wxPaintEvent& event);
    void OnKeyDown(wxKeyEvent& event);
	void OnKeyUp(wxKeyEvent& event);
	void OnSize(wxSizeEvent& event);

protected:
    DECLARE_EVENT_TABLE()
	bool m_VehicleForward;
	bool m_VehicleBackward;
	bool m_VehicleLeft;
	bool m_VehicleRight;
	bool m_VehicleHandbrake;
};

#endif // __MAIN_H__